package lab.ggoma.chat;


import com.github.nkzawa.socketio.client.Socket;

/**
 * Created by seong-uim on 2017. 6. 1..
 */

public class GGomaChatHelper {

    private static final long SEND_MIN_TIME = 2 * 1000L;
    private static long mSentTime = 0L;

    public static boolean checkValidSocket(Socket socket) {
        if (socket == null || !socket.connected()) {
            return false;
        }
        return true;
    }
    public static boolean checkTime() {
        boolean isTimeOver = System.currentTimeMillis() - mSentTime > SEND_MIN_TIME;
        if (isTimeOver) {
            mSentTime = System.currentTimeMillis();
        }
        return isTimeOver;
    }
    public static boolean checkValidParam(String userNo, String userNick, String thumbUrl) {
        if (userNo == null || userNo.isEmpty()) {
            return false;
        }
        if (userNick == null || userNick.isEmpty()) {
            return false;
        }
        if (thumbUrl == null || thumbUrl.isEmpty()) {
            return false;
        }
        return true;
    }

}
