package lab.ggoma.chat;


/**
 * Created by seong-uim on 2017. 5. 30..
 */

public class GGomaChatConstant {
    public static String SERVER_PORT = "8253";
    public static String SERVER_IP = "192.168.0.130"; // 성우.
    //public static final String SERVER_IP = "192.168.0.2"; // 기수
    public static final String AUTHKEY = "R#s/zdW-xC*{&7!Z";
    public static final String SERVER_HTTP_BASIC_AUTH = "ggomalab:7pdlaj2015";
    public static final String DEFAULT_GUEST_USERNO = "0"; //로그인 되지않은 사용자 또는 어드민 메시지
    public enum EventType {
        CONT,
        ERROR,
        HELO,
        JOIN,
        MESG,
        RELY,
        USER,
        ROOM,
        EVNT,
        ROKT,
        ITEM,
        ADMN,
        QUIT
    };
}
