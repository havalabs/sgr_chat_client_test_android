package lab.ggoma.chat;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.test003.ggoma.sgr_chat_client_test.utils.GGomaLog;

import org.json.JSONObject;

import java.net.URI;

import lab.ggoma.chat.GGomaChatConstant.EventType;

/**
 * Created by seong-uim on 2017. 5. 25..
 */

public class GGomaChatClient {
    public interface IGGomaChatClient {
        public void onChatEvent(GGomaChatClient client, GGomaChatStatusStore status, GGomaEvent event);
    }
    public class GGomaEvent {
        public EventType type;
        public JSONObject payload;
        public GGomaEvent(EventType type, JSONObject data){
            this.type = type;
            this.payload = data;
        }
        public String toString(){
            return "type " + this.type.name() + "  : payload " + ((this.payload != null) ? this.payload.toString() + " ": " ");
        }
    }
    private final String TAG = "GGOMA";
    private Socket client = null;
    private IGGomaChatClient mInterface = null;
    private GGomaChatStatusStore mStatusStore = new GGomaChatStatusStore();
    public GGomaChatClient(){
    }
    public void setInterface(IGGomaChatClient iggomaChatClient){
        mInterface = iggomaChatClient;
    }
    public void setMyInfo(String no, String id, String key, String deviceid){
        mStatusStore.setMyUserNo(no);
        mStatusStore.setUserId(id);
        mStatusStore.setUserKey(key);
        mStatusStore.setDeviceKey(deviceid);
    }
    public boolean connect(String host, String port, String roomno) {
        if (host == null || host.isEmpty() ||
                port == null || port.isEmpty() ||
                roomno == null || roomno.isEmpty())
        {
            return false;
        }
        if (client != null) {
            this.disconnect();
        }
        if(mStatusStore != null){
            mStatusStore.clear();
        }
        try {
            IO.Options opts = new IO.Options();
            //opts.forceNew = true;
            //opts.reconnection = true;
            opts.timeout = 15000;
            String url = "http://"+host+":"+port;
            GGomaLog.v(TAG, "GGomaChatClient url : " + url);
            client = IO.socket(URI.create(url), opts);
            GGomaLog.d("GGOMA","GGomaChatClient connecting..");
            client.connect();
        } catch (Exception e) {
            GGomaLog.v(TAG, "GGomaChatClient connect Exception : " + e.toString());
            return false;
        }
        mStatusStore.setRoomNo(roomno);
        setEventListener();
        return true;
    }
    public void disconnect() {
        if (client != null) {
            GGomaLog.d("GGOMA", "GGomaChatClient disconnect");
            client.disconnect();
            for(final EventType type :EventType.values()){
                client.off(type.name());
            }
            client.close();
            client = null;
        }
        if(mStatusStore != null){
            GGomaLog.d("GGOMA", "GGomaChatClient user clear");
            mStatusStore.clear();
        }
        mInterface = null;
    }
    public GGomaEvent createEvent(EventType type, JSONObject data){
        return new GGomaEvent(type, data);
    }
    public GGomaChatStatusStore getChatStatusStore(){
        return mStatusStore;
    }
    public boolean sendEvent(GGomaEvent event ){
        if (GGomaChatHelper.checkValidSocket(client)) {
            try {
                GGomaLog.d(TAG, "emit send : " + event.type.name() + " " + event.payload.toString());
                client.emit(event.type.name(), event.payload);
                return true;
            } catch (Exception e) {
                GGomaLog.d(TAG, "sendEvent Exception : " + e.toString());
            }
        }
        return false;
    }
    private void setEventListener() {
        client.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if(mInterface != null) {
                    GGomaEvent event = createEvent(EventType.CONT, null);
                    mInterface.onChatEvent(GGomaChatClient.this, mStatusStore, event);
                }
            }
        }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if(mInterface != null) {
                    GGomaEvent event = createEvent(EventType.ERROR, null);
                    mInterface.onChatEvent(GGomaChatClient.this, mStatusStore, event);
                }
            }
        });
        for(final EventType type : EventType.values()){
            client.on(type.name(), new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    try {
                        JSONObject obj = (JSONObject) args[0];
                        if(obj == null){
                            GGomaLog.d("GGOMA", "Socket.io call null");
                        }
                        if(!GGomaChatPacket.checkVaild(type, obj, mStatusStore)){
                            GGomaLog.d("GGOMA", "Socket.io call checkVaild false : " + type.name() + " " + obj.toString());
                            return;
                        }
                        GGomaChatPacket.parse(type, obj, mStatusStore);
                        if (mInterface != null) {
                            GGomaEvent event = createEvent(type, obj);
                            mInterface.onChatEvent(GGomaChatClient.this, mStatusStore, event);
                        }
                    } catch (Exception e) {
                        GGomaLog.d("GGOMA", "Socket.io call Exception : " + type.name() + " " + e.toString());
                    }
                }
            });
        }
    }
}
