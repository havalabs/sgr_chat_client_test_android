package lab.ggoma.chat;

import com.test003.ggoma.sgr_chat_client_test.utils.GGomaLog;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by seong-uim on 2017. 6. 1..
 */

public class GGomaChatStatusStore {
    private String mUpcount = "0";
    private String mUsercount = "0";
    private String mBJUserno = "";
    private String mMyUserno = "";
    private String mRoomNo = "";
    private String mMyUserId = "";
    private String mMyUserKey = "";
    private String mMyDeviceKey = "";
    private boolean mFrozen = false;
    private boolean mVoting = false;
    private boolean mShutup = false;
    private boolean mExternalChatBlock = false;
    private HashMap<String, JSONObject> chatUsers = new HashMap<String, JSONObject>();
    private HashMap<String, JSONObject> externalChatUsers = new HashMap<String, JSONObject>();

    public boolean getIsBJMode(){
        return mMyUserno.equals(mBJUserno);
    }

    public void setUserId(String userid){
        mMyUserId = userid;
    }
    public String getUserId(){
        return mMyUserId;
    }
    public void setUserKey(String userkey){
        mMyUserKey = userkey;
    }
    public String getUserKey(){
        return mMyUserKey;
    }
    public void setDeviceKey(String devicekey){
        mMyDeviceKey = devicekey;
    }
    public String getDeviceKey(){
        return mMyDeviceKey;
    }
    public void setRoomNo(String roomno){
        mRoomNo = roomno;
    }
    public String getRoomNo(){
        return mRoomNo;
    }
    public void setMyUserNo(String userno){
        mMyUserno = userno;
    }
    public String getmMyUserno(){
        return mMyUserno;
    }
    public boolean getIsFrozen(){
        return mFrozen;
    }
    public void setFrozen(boolean value){
        mFrozen = value;
    }
    public boolean getIsShutup(){
        return mShutup;
    }
    public void setShutup(boolean value){
        mShutup = value;
    }
    public boolean getIsVoting(){
        return mVoting;
    }
    public void setVoting(boolean value){
        mVoting = value;
    }
    public boolean getIsExternalChatBlock(){
        return mExternalChatBlock;
    }
    public void setExternalChatBlock(boolean value){
        mExternalChatBlock = value;
    }




    public String getmUpcount(){
        return mUpcount;
    }
    public void setmUpcount(String count){
        mUpcount = count;
    }
    public String getUserCount(){
        return mUsercount = String.valueOf(chatUsers.keySet().size());
    }
    public JSONObject getBJInfo(){
        return chatUsers.get(mBJUserno);
    }
    public JSONObject getMyInfo(){
        return chatUsers.get(mMyUserno);
    }


    // SGR 채팅 유저.
    public void addUser(String userno, JSONObject userData){
        chatUsers.put(userno, userData);
        String role = userData.optString("role");
        if(role.equals("b")){
            mBJUserno = userno;
        }
    }
    public void removeUser(String userno){
        chatUsers.remove(userno);
    }
    public ArrayList<JSONObject> getUserList(){
        ArrayList<JSONObject> users = new ArrayList<JSONObject>();
        for(String userno : chatUsers.keySet()){
            users.add(chatUsers.get(userno));
        }
        return users;
    }
    public void setManagerAssign(String userno, boolean assign){
        try{
            JSONObject userObj = chatUsers.get(userno);
            userObj.put("role",assign ? "m":"g");
            addUser(userno, userObj);
        }catch (Exception e){
            GGomaLog.d("GGOMA","setManagerAssign Exception : " + e.toString());
        }
    }
    public ArrayList<JSONObject> getManagerList(){
        ArrayList<JSONObject> managers = new ArrayList<JSONObject>();
        for(String userno : chatUsers.keySet()){
            if(chatUsers.get(userno).optString("role").equals("m")){
                managers.add(chatUsers.get(userno));
            }
        }
        return managers;
    }


    // 외부 채팅 유저 관리.
    public void addExternalUser(String userno, JSONObject userData){
        externalChatUsers.put(userno, userData);
    }
    public void removeExternalUser(String userno){
        externalChatUsers.remove(userno);
    }
    public JSONObject getExternalUser(String userno){
        return externalChatUsers.get(userno);
    }
    public boolean isExistExternalUser(String userno){
        return externalChatUsers.containsKey(userno);
    }







    public String getAuthKey(){
        try{
            String text = getUserId()+"|"+getUserKey()+"|"+getDeviceKey()+"|"+GGomaChatConstant.AUTHKEY;
            StringBuffer sbuf = new StringBuffer();
            MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(text.getBytes());
            byte[] msgStr = mDigest.digest();
            String hexString = new String(Hex.encodeHex(msgStr));
            return hexString.toString() ;
        }catch (Exception e) {
            GGomaLog.d("GGOMA", "getAuthKey Exception : " + e.toString());
        }
        return null;
    }
    public void clear(){
        mUpcount = "0";
        mUsercount = "0";
        mBJUserno = "";
        mMyUserno = "";
        mRoomNo = "";
        mMyUserId = "";
        mMyUserKey = "";
        mMyDeviceKey = "";
        mFrozen = false;
        mVoting = false;
        mShutup = false;
        mExternalChatBlock = false;

        chatUsers.clear();
        externalChatUsers.clear();;
    }
}
