package lab.ggoma.chat;



import org.json.JSONArray;
import org.json.JSONObject;

import lab.ggoma.chat.GGomaChatConstant.EventType;


/**
 * Created by seong-uim on 2017. 6. 1..
 */

public class GGomaChatPacket {
    public static boolean checkVaild(EventType type, JSONObject data, final GGomaChatStatusStore store){
        if(data == null){
            return false;
        }
        // 이벤트 별로 정해진 key:value가 있는지 검사
        boolean isVaild = true;
        switch (type) {
            case MESG: {
                if (!data.has("userno") || !data.has("msg") || !data.has("date")) {
                    isVaild = false;
                }
                break;
            }
            case RELY: {
                // 외부채팅 블럭인 경우 처리.
                if(store.getIsExternalChatBlock()){
                    isVaild = false;
                }
                if (!data.has("data") || !data.has("date")) {
                    isVaild = false;
                }
                break;
            }
            case HELO: {
                if (!data.has("room") || !data.has("upcount") || !data.has("users") || !data.has("attr")) {
                    isVaild = false;
                }
                break;
            }
            case JOIN: {
                if(data.has("error")){ // 에러가 있을경우는 바로,
                    break;
                }
                if (!data.has("userno") || !data.has("usernick") || !data.has("thumburl") || !data.has("role") ) {
                    isVaild = false;
                }
                break;
            }
            case QUIT: {
                if (!data.has("userno")) {
                    isVaild = false;
                }
                break;
            }
            case ROKT:{
                if (!data.has("count")) {
                    isVaild = false;
                }
            }
            case ROOM:{
                if (!data.has("attr")){
                    if(data.optString("attr").equals("+v")){
                        if(!data.has("data")){
                            isVaild = false;
                        }
                    }
                }
                break;
            }
            case USER:{
                if (!data.has("userno") || !data.has("attr")) {
                    isVaild = false;
                }
                break;
            }
            case ITEM:{
                if (!data.has("type") || !data.has("data")) {
                    isVaild = false;
                }
                break;
            }
            case EVNT:{
                if (!data.has("type") || !data.has("data")) {
                    isVaild = false;
                }
                break;
            }

        }
        return isVaild;
    }
    public static void parse(final EventType type, final JSONObject data, final GGomaChatStatusStore store){
        switch (type){
            case MESG:
            case RELY:{
                break;
            }
            case HELO:{
                JSONArray users = data.optJSONArray("users");
                for (int j = 0; j < users.length(); j++) {
                    JSONObject user = users.optJSONObject(j);
                    store.addUser(user.optString("userno"), user);
                }
                String attr = data.optString("attr");
                if(attr.contains("f")){
                    store.setFrozen(true);
                }
                if(attr.contains("v")){
                    store.setVoting(true);
                }
                store.setmUpcount(data.optString("upcount"));
                break;
            }
            case JOIN:{
                store.addUser(data.optString("userno"), data);
                break;
            }
            case QUIT:{
                store.removeUser(data.optString("userno"));
                break;
            }
            case ROKT:{
                store.setmUpcount(data.optString("count"));
                break;
            }
            case EVNT:{
                if(data.has("type")){
                    String eventType = data.optString("type");
                    switch (eventType){
                        case "recomm":{ // 추천하기.
                            String userno = data.optJSONObject("data").optString("userno");
                            break;
                        }
                        case "follow":{ // 팔로우.
                            String userno = data.optJSONObject("data").optString("userno");
                            break;
                        }
                        case "notice":{ // 기타 이벤트.
                            JSONObject notice = data.optJSONObject("data");
                            break;
                        }
                    }
                }
                break;
            }
            case ITEM:{
                if(data.has("type")){
                    String itemType = data.optString("type");
                    switch (itemType){
                        case "candy":{
                            String count = data.optJSONObject("data").optString("count");
                            break;
                        }
                        case "megaphone":{
                            JSONObject megaphone = data.optJSONObject("data");
                            // {“type”:”megaphone”, “data”:{“userno”: “1234”, “usernick”:””, “thumburl”:”http://...”, “msg”:”메세지 텍스트”}}
                            break;
                        }
                    }
                }
                break;
            }
            case USER:{
                if(data.has("attr") && data.has("userno")){
                    String attr = data.optString("attr");
                    switch (attr){
                        case "+s":{ // 벙어리
                            if(data.optString("userno").equals(store.getmMyUserno())){
                                store.setShutup(true); // 자기 자신이라면, 설정.
                            }
                            break;
                        }
                        case "+k":{ // 강퇴
                            store.removeUser(data.optString("userno"));
                            // 자기 자신이라면, 강퇴되어야 한다.
                            break;
                        }
                        case "+m":{
                            store.setManagerAssign(data.optString("userno"), true);
                            break;
                        }
                        case "-m":{
                            // 매니저 해임.
                            store.setManagerAssign(data.optString("userno"), false);
                            break;
                        }
                    }
                }
                break;
            }
            case ROOM:{
                if(data.has("attr")){
                    String attr = data.optString("attr");
                    switch (attr){
                        case "+f":{ // 얼리기.
                            store.setFrozen(true);
                            break;
                        }
                        case "-f":{ // 녹이기.
                            store.setFrozen(false);
                            break;
                        }
                        case "+v":{ // 투표시작.
                            JSONObject vote = data.optJSONObject("data");
                            store.setVoting(true);
                            break;
                        }
                        case "-v":{ // 투표종료.
                            store.setVoting(false);
                            break;
                        }
                    }
                }
                break;
            }
            case ADMN:{
                // 어드민
                if(data.has("type")){
                    String adminType = data.optString("type");
                    switch (adminType){
                        case "msg":{
                            // 어드민 메시지.
                            break;
                        }
                    }
                }
                break;
            }
        }
    }
}
