package com.test003.ggoma.sgr_chat_client_test.utils;

import android.os.AsyncTask;
import android.util.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import lab.ggoma.chat.GGomaChatConstant;

/**
 * Created by seong-uim on 2017. 5. 30..
 */

public class HttpAsyncTask extends AsyncTask<String,  Void, String> {
    public interface IHttpAsyncTask {
        public void onSuccess(JSONObject data);
    }
    private IHttpAsyncTask mInterface = null;
    public HttpAsyncTask(IHttpAsyncTask inter){
        mInterface = inter;
    }
    @Override
    protected String doInBackground(String... urls) {
        String response = null;
        try{
            InputStream inputStream= getInputStreamFromUrl(urls);
            if(inputStream != null) {
                response = convertInputStreamToString(inputStream);
            }
            //GGomaLog.d("GGOMA", "convertInputStreamToString response : " +  response);
        }catch (Exception e){
            GGomaLog.d("GGOMA", "convertInputStreamToString : " +  e);
        }
        return response;
    }
    @Override
    protected void onPostExecute(String result) {
        if(result != null && mInterface != null){
            try{
                JSONObject data = new JSONObject(result);
                if(data != null) {
                    mInterface.onSuccess(data);
                }
            }catch (Exception e){
                GGomaLog.d("GGOMA","HttpAsyncTask onPostExecute : " + e.toString());
            }
        }
    }
    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }
    private InputStream getInputStreamFromUrl(String... urls) {
        InputStream content = null;
        try{
            String basicAuth = "Basic " + Base64.encodeToString(GGomaChatConstant.SERVER_HTTP_BASIC_AUTH.getBytes(), Base64.NO_WRAP);
            HttpClient httpclient = new DefaultHttpClient();
            if(urls[0].equals("GET")){
                String fullURL = urls[1];
                if(!urls[2].isEmpty()) {
                    fullURL = urls[1] + "?" + urls[2];
                }

                // URLEncoder.encode(latVal, "UTF-8")


                GGomaLog.d("GGOMA","GET URL : " + fullURL);
                HttpGet httpget = new HttpGet(fullURL);
                httpget.setHeader("Authorization", basicAuth);
                HttpResponse response = httpclient.execute(httpget);
                content = response.getEntity().getContent();
            }else if(urls[0].equals("POST")){
                HttpPost httppost = new HttpPost(urls[1]);
                httppost.setHeader("Authorization", basicAuth);
                String querys[] = urls[2].split("&");
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                for(String query : querys){
                    //GGomaLog.d("GGOMA","query : " + query);
                    String key = query.split("=")[0];
                    String value = query.split("=")[1];
                    GGomaLog.d("GGOMA","POST key : " + key + "| value : " + value);
                    //params.add(new BasicNameValuePair(query.split("=")[0], query.split("=")[1]));
                    params.add(new BasicNameValuePair(key, value));
                }
                UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
                httppost.setEntity(ent);
                HttpResponse response = httpclient.execute(httppost);
                content = response.getEntity().getContent();
            }
        } catch (Exception e) {
            GGomaLog.d("GGOMA", "Network exception " + e.toString());
        }
        return content;
    }
}