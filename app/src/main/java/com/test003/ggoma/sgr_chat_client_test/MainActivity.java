package com.test003.ggoma.sgr_chat_client_test;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.test003.ggoma.sgr_chat_client_test.utils.GGomaLog;
import com.test003.ggoma.sgr_chat_client_test.utils.HttpAsyncTask;
import com.test003.ggoma.sgr_chat_client_test.utils.RequestHelper;
import com.test003.ggoma.sgr_chat_client_test.view.TestViewController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;

import lab.ggoma.chat.GGomaChatClient;
import lab.ggoma.chat.GGomaChatConstant;
import lab.ggoma.chat.GGomaChatStatusStore;

import static lab.ggoma.chat.GGomaChatConstant.EventType.JOIN;
import static lab.ggoma.chat.GGomaChatConstant.EventType.MESG;
import static lab.ggoma.chat.GGomaChatConstant.EventType.RELY;
import static lab.ggoma.chat.GGomaChatConstant.EventType.ROKT;
import static lab.ggoma.chat.GGomaChatConstant.EventType.ROOM;
import static lab.ggoma.chat.GGomaChatConstant.EventType.USER;


public class MainActivity extends AppCompatActivity {
    private GGomaChatClient chatClient;
    private TestViewController viewController = null;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chat();
        view();
        setActivityTitle();
    }
    @Override
    protected void onResume(){
        super.onResume();
        GGomaLog.d("GGOMA", "MainActivity onResume");
        showRoomlist();
    }
    @Override
    protected void onPause(){
        super.onPause();
        GGomaLog.d("GGOMA", "MainActivity onPause");
        if(chatClient != null){
            GGomaLog.d("GGOMA", "MainActivity onPause disconnect");
            chatClient.disconnect();
        }
        finish();
    }
    public void view(){
        viewController = new TestViewController(this, chatClient);
        viewController.setInterface(new TestViewController.IGGomaChatViewController() {
            @Override
            public void onViewEvent(TestViewController.ViewEventType type, Object... args){
                try {
                    switch (type) {
                        case SELECT: { // 메세지 이름 변경 필요.
                            final String userno = (String) args[0];
                            final String roomno = (String) args[1];
                            if (userno.equals(GGomaChatConstant.DEFAULT_GUEST_USERNO)) {
                                chatClient.setMyInfo(GGomaChatConstant.DEFAULT_GUEST_USERNO, "", "", "");
                                chatClient.connect(GGomaChatConstant.SERVER_IP, GGomaChatConstant.SERVER_PORT, roomno);
                            } else {
                                RequestHelper.requestUserInfo(userno, new HttpAsyncTask.IHttpAsyncTask() {
                                    @Override
                                    public void onSuccess(JSONObject data) {
                                        String targetRoomNo = roomno;
                                        JSONObject resultObj = data.optJSONObject("result");
                                        if (targetRoomNo == null) {
                                            // 개설모드. 이전 VOD 방송의 룸 넘버를 가져옴.
                                            targetRoomNo = resultObj.optJSONObject("vod_info").optString("no");
                                        }
                                        String id = resultObj.optString("id");
                                        String key = resultObj.optString("user_key");
                                        String deviceId = resultObj.optString("device_id");
                                        chatClient.connect(GGomaChatConstant.SERVER_IP, GGomaChatConstant.SERVER_PORT, targetRoomNo);
                                        chatClient.setMyInfo(userno, id, key, deviceId);
                                        setActivityTitle();
                                    }
                                });
                            }

                            break;
                        }
                        case MESSAGE: {
                            String userno = (String) args[0];
                            String message = (String) args[1];
                            JSONObject payload = new JSONObject();
                            payload.put("userno", userno);
                            payload.put("msg", message);
                            chatClient.sendEvent(chatClient.createEvent(MESG, payload));
                            break;
                        }
                        case EXTERNAL_MESSAGE: {
                            String message = (String) args[0];
                            JSONObject payload = new JSONObject();
                            payload.put("type", "youtube");
                            payload.put("userno", "UCALShuUIuFddbx6LU4644rg");  // youtube channelid
                            payload.put("usernick", "임성우"); // displayname
                            payload.put("thumburl", "https://yt3.ggpht.com/-d_Q9UTzliQo/AAAAAAAAAAI/AAAAAAAAAAA/lgtOFZKyas0/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"); // profileImageUrl
                            payload.put("msg", message); // display message
                            chatClient.sendEvent(chatClient.createEvent(RELY, payload));
                            break;
                        }
                        case ROKET: {
                            String count = (String) args[0];
                            JSONObject payload = new JSONObject();
                            payload.put("count", Integer.parseInt(count));
                            chatClient.sendEvent(chatClient.createEvent(ROKT, payload));
                            break;
                        }
                        case FROZEN: {
                            boolean enable = (boolean) args[0];
                            //chatClient.sendFrozen(isFrozen);
                            JSONObject payload = new JSONObject();
                            payload.put("attr", enable ? "+f":"-f");
                            chatClient.sendEvent(chatClient.createEvent(ROOM, payload));
                            break;
                        }
                        case KICK: {
                            String userno = (String) args[0];
                            JSONObject payload = new JSONObject();
                            payload.put("userno", userno);
                            payload.put("attr", "+k");
                            chatClient.sendEvent(chatClient.createEvent(USER, payload));
                            break;
                        }
                        case SHUTUP: {
                            String userno = (String) args[0];
                            JSONObject payload = new JSONObject();
                            payload.put("userno", userno);
                            payload.put("attr", "+s");
                            chatClient.sendEvent(chatClient.createEvent(USER, payload));
                            break;
                        }
                        case RECOMM: {
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&data=" +new JSONObject().put("userno",chatClient.getChatStatusStore().getmMyUserno()).toString();
                            RequestHelper.requestEvent("POST", "/events/recomm", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case FOLLOW: {
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&data=" +new JSONObject().put("userno",chatClient.getChatStatusStore().getmMyUserno()).toString();
                            RequestHelper.requestEvent("POST", "/events/follow", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case MEGAPHONE: {
                            String message = (String) args[0];
                            JSONObject megaphone = new JSONObject();
                            megaphone.put("userno", chatClient.getChatStatusStore().getmMyUserno());
                            megaphone.put("usernick", chatClient.getChatStatusStore().getMyInfo().opt("usernick"));
                            megaphone.put("thumburl", "https://i.ytimg.com/vi/vYuvwF7kLTw/hqdefault_live.jpg");
                            megaphone.put("msg", message);

                            String queryString = "room=0&data=" + URLEncoder.encode(megaphone.toString(), "UTF-8");
                            RequestHelper.requestEvent("GET", "/items/megaphone", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case CANDY:{
                            String count = (String) args[0];
                            JSONObject candyCount = new JSONObject();
                            candyCount.put("count", Integer.parseInt(count));
                            candyCount.put("userno", chatClient.getChatStatusStore().getmMyUserno());
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo()
                                    + "&data=" + URLEncoder.encode(candyCount.toString(), "UTF-8");

                            RequestHelper.requestEvent("GET", "/items/candy", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });

                            break;
                        }
                        case VOTE_START: {
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&voting=1";
                            RequestHelper.requestEvent("GET","/vote", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case VOTE_END: {
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&voting=0";
                            RequestHelper.requestEvent("GET", "/vote", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case MANAGER_SET: {
                            String userno = (String) args[0];
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&userno=" + userno +"&val=1";
                            RequestHelper.requestEvent("GET", "/chat/manager", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case MANAGER_UNSET: {
                            String userno = (String) args[0];
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&userno=" + userno +"&val=0";
                            RequestHelper.requestEvent("GET", "/chat/manager", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case NOTICE: {
                            String noticeMessage = (String) args[0];
                            JSONObject noticeObj = new JSONObject();
                            noticeObj.put("msg",noticeMessage);
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&data=" + noticeObj.toString();
                            RequestHelper.requestEvent("POST", "/events/notice", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });

                            break;
                        }
                        case ADMIN_MSG: {
                            String adminMessage = (String) args[0];
                            JSONObject adminMessageObj = new JSONObject();
                            adminMessageObj.put("thumburl","https://i.ytimg.com/vi/vYuvwF7kLTw/hqdefault_live.jpg");
                            adminMessageObj.put("text",adminMessage);
                            adminMessageObj.put("color","ff22ff");
                            adminMessageObj.put("userno","0");

                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&type=msg&data=" + adminMessageObj.toString();
                            RequestHelper.requestEvent("POST", "/admin", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });
                            break;
                        }
                        case ADMIN_KICK: {
                            String userno = (String) args[0];
                            String reason = URLEncoder.encode("이러저러한 사유로 " + viewController.getCharUser(userno).getName() + " 어드민 강퇴", "UTF-8");
                            String queryString = "room=" + chatClient.getChatStatusStore().getRoomNo() + "&userno=" + userno + "&reason=" + reason;
                            RequestHelper.requestEvent("GET", "/chat/kick", queryString, new HttpAsyncTask.IHttpAsyncTask() {
                                @Override
                                public void onSuccess(JSONObject data) {}
                            });


                            break;
                        }
                    }
                } catch (Exception e) {
                    GGomaLog.d("GGOMA", "onViewEvent Exception : " + e.toString());

                }
            }
        });
    }
    public void chat(){
        chatClient = new GGomaChatClient();
        chatClient.setInterface(new GGomaChatClient.IGGomaChatClient() {
            @Override
            public void onChatEvent(GGomaChatClient client, GGomaChatStatusStore store,  GGomaChatClient.GGomaEvent event) {
                GGomaLog.d("GGOMA", "onChatEvent : " + event.toString());
                try {
                    switch (event.type) {
                        case CONT: {
                            JSONObject payload = new JSONObject();
                            payload.putOpt("room", store.getRoomNo());
                            if(!store.getmMyUserno().equals(GGomaChatConstant.DEFAULT_GUEST_USERNO)) {
                                payload.putOpt("user_id", store.getUserId());
                                payload.putOpt("auth_key", store.getAuthKey());
                            }
                            chatClient.sendEvent(chatClient.createEvent(JOIN, payload));
                            break;
                        }
                        case HELO:{
                            JSONArray users = event.payload.optJSONArray("users");
                            for(int i = 0; i < users.length(); i++ ){
                                viewController.addChatUser((JSONObject)users.get(i));
                            }
                            setActivityTitle();
                            break;
                        }
                        case JOIN:{
                            if(event.payload.has("error")){
                                viewController.addRoomInfoMessage("채팅방 입장 에러 :" + event.payload.optString("error"));
                            }else {
                                viewController.addChatUser(event.payload);
                                setActivityTitle();
                            }
                            break;
                        }
                        case QUIT:{
                            viewController.removeChatUser(event.payload);
                            setActivityTitle();
                            break;
                        }
                        case MESG:{
                            viewController.addChatMessage(event.payload);
                            break;
                        }
                        case RELY:{
                            viewController.addChatExternalMessage(event.payload);
                            break;
                        }
                        case ROKT: {
                            viewController.addRoomInfoMessage("로켓 이벤트 갯수:" + event.payload.optInt("count"));
                            setActivityTitle();
                            break;
                        }
                        case USER:{
                            switch (event.payload.optString("attr")){
                                case "+s":{ // 벙어리.
                                    viewController.addRoomInfoMessage("벙어리 지정:" + viewController.getCharUser(event.payload.optString("userno")).getName());
                                    break;
                                }
                                case "+k":{ // 강제 퇴장.
                                    viewController.addRoomInfoMessage("강퇴 : " + viewController.getCharUser(event.payload.optString("userno")).getName());
                                    if(event.payload.has("reason")){
                                        viewController.addRoomInfoMessage("사유 : " + event.payload.optString("reason"));
                                    }

                                    if(event.payload.optString("userno").equals(store.getmMyUserno())){
                                        finish();
                                    }
                                    break;
                                }
                                case "+m":{
                                    viewController.setManagerCharUser(true, event.payload.optString("userno"));
                                    viewController.addRoomInfoMessage("매니저 임명 : " + viewController.getCharUser(event.payload.optString("userno")).getName());
                                    break;
                                }
                                case "-m":{
                                    viewController.setManagerCharUser(false, event.payload.optString("userno"));
                                    viewController.addRoomInfoMessage("매니저 해임 : " + viewController.getCharUser(event.payload.optString("userno")).getName());
                                    break;
                                }
                            }
                            break;
                        }
                        case ROOM:{
                            switch (event.payload.optString("attr")){
                                case "+v":{
                                    viewController.addRoomInfoMessage("투표 시작 이벤트");
                                    break;
                                }
                                case "-v":{
                                    viewController.addRoomInfoMessage("투표 종료 이벤트");
                                    break;
                                }
                                case "+f":{
                                    viewController.addRoomInfoMessage("방 얼림 이벤트");
                                    break;
                                }
                                case "-f":{
                                    viewController.addRoomInfoMessage("방 녹임 이벤트");
                                    break;
                                }

                            }
                            setActivityTitle();
                            break;
                        }
                        case EVNT:{
                            switch (event.payload.optString("type")){
                                case "notice":{
                                    viewController.addRoomInfoMessage("공지메시지:" + event.payload.optJSONObject("data").optString("msg"));
                                    return;
                                }
                            }
                            viewController.addEventMessage(event.payload);
                            break;
                        }
                        case ITEM:{
                            switch (event.payload.optString("type")) {
                                case "megaphone": {
                                    viewController.addRoomInfoMessage("확성기:" + event.payload.optJSONObject("data").optString("msg"));
                                    break;
                                }
                                case "candy": {
                                    String name = viewController.getCharUser(event.payload.optJSONObject("data").optString("userno")).getName();
                                    int count = event.payload.optJSONObject("data").optInt("count");
                                    viewController.addRoomInfoMessage("사탕쏘기:" + name + " " + count + " 쏨");
                                    break;
                                }
                            }
                            break;
                        }
                        case ADMN:{
                            switch (event.payload.optString("type")){
                                case "msg":{
                                    viewController.addRoomInfoMessage("어드민메시지:" + event.payload.optJSONObject("data").optString("text"));
                                    break;
                                }
                            }
                            break;
                        }
                        case ERROR: {
                            viewController.errorMessageHandling(event.payload);
                            break;
                        }
                    }
                } catch (Exception e) {
                    GGomaLog.d("GGOMA", "onChatEvent Exception : " + e.toString());
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, "채팅룸리스트");
        menu.add(0, 1, 1, "YouTube챗_ON");
        menu.add(0, 2, 2, "YouTube챗_OFF");
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item != null){
            switch (item.getTitle().toString()){
                case "채팅룸리스트": {
                    chatClient.disconnect();
                    // viewController
                    showRoomlist();
                    break;
                }
                case "YouTube챗_ON": {
                    chatClient.getChatStatusStore().setExternalChatBlock(false);
                    break;
                }
                case "YouTube챗_OFF": {
                    chatClient.getChatStatusStore().setExternalChatBlock(true);
                    break;
                }
            }
            Toast.makeText(this, item.getTitle() + " selected", Toast.LENGTH_SHORT).show();

            return true;
        } else {
            return false;
        }
    }
    private void showRoomlist(){
        RequestHelper.requestRoomList(new HttpAsyncTask.IHttpAsyncTask() {
            @Override
            public void onSuccess(JSONObject data) {
                if(data != null) {
                    GGomaLog.d("GGOMA", "MainActivity onResume requestRoomList : " + data.toString());
                    viewController.setupChatView();
                    viewController.popupRoomList(data);
                }
            }
        });
    }

    private void setActivityTitle(){
        if(chatClient != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    String roomno = chatClient.getChatStatusStore().getRoomNo();
                    String usercnt = chatClient.getChatStatusStore().getUserCount();
                    String upcnt = chatClient.getChatStatusStore().getmUpcount();
                    String isFrozen = chatClient.getChatStatusStore().getIsFrozen() ? "O" : "X";
                    String isVote = chatClient.getChatStatusStore().getIsVoting() ? "O" : "X";
                    String isShutup = chatClient.getChatStatusStore().getIsShutup() ? "O" : "X";
                    //setTitle(String.format("%s|유저:%s|업:%s|얼림:%s|투표:%s|쉿:%s", roomno, usercnt, upcnt,isFrozen, isVote, isShutup));
                    setTitle(String.format("유저:%s|업:%s|얼림:%s|투표:%s|쉿:%s", usercnt, upcnt,isFrozen, isVote, isShutup));
                }
            });
        }
    }

}
