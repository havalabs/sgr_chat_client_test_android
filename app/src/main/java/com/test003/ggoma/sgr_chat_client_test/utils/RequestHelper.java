package com.test003.ggoma.sgr_chat_client_test.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;

import lab.ggoma.chat.GGomaChatConstant;

public class RequestHelper {

    public static void requestUserInfo(String userNo, HttpAsyncTask.IHttpAsyncTask iHttpAsyncTask){
        String requestUrl = "http://streetgamer.tv/api/user/get_user_info";
        GGomaLog.d("GGOMA","requestUserInfo requestUrl : " + requestUrl);
        new HttpAsyncTask(iHttpAsyncTask).execute("GET", requestUrl, "user_no=" + userNo);
    }
    public static void requestRoomList(HttpAsyncTask.IHttpAsyncTask iHttpAsyncTask) {
        String requestUrl = "http://"+ GGomaChatConstant.SERVER_IP + ":"+GGomaChatConstant.SERVER_PORT + "/api/room";
        GGomaLog.d("GGOMA","requestRoomList requestUrl : " + requestUrl);
        new HttpAsyncTask(iHttpAsyncTask).execute("GET", requestUrl, "");
    }

    public static void requestEvent(String method, String eventString, String queryString, HttpAsyncTask.IHttpAsyncTask iHttpAsyncTask) {
        String requestUrl = "http://"+ GGomaChatConstant.SERVER_IP + ":"+GGomaChatConstant.SERVER_PORT + "/api" + eventString;
        GGomaLog.d("GGOMA","requestEvent method : " + method +  " requestUrl : " + requestUrl + " query : " + queryString);
        new HttpAsyncTask(iHttpAsyncTask).execute(method,requestUrl, queryString);
    }




    public static void requestManger(boolean isAssign, String roomno, String userno,  HttpAsyncTask.IHttpAsyncTask iHttpAsyncTask) {
//        String requestUrl = "http://"+GGomaChatConstant.SERVER_IP + ":"+GGomaChatConstant.SERVER_PORT + "/api/chat/manager?room=" + roomno + "&userno=" + userno + "&val=" + ((isAssign == true) ? 1 :0);
//        GGomaLog.d("GGOMA","requestManger requestUrl : " + requestUrl);
//        new HttpAsyncTask(iHttpAsyncTask).execute(requestUrl);
    }



    public static String getEncMD5(String txt) throws Exception {
        StringBuffer sbuf = new StringBuffer();
        MessageDigest mDigest = MessageDigest.getInstance("MD5");
        mDigest.update(txt.getBytes());
        byte[] msgStr = mDigest.digest();
        String hexString = new String(Hex.encodeHex(msgStr));
        return hexString.toString() ;
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
}