package com.test003.ggoma.sgr_chat_client_test.view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.github.bassaer.chatmessageview.models.Message;
import com.github.bassaer.chatmessageview.models.User;
import com.github.bassaer.chatmessageview.views.ChatView;
import com.test003.ggoma.sgr_chat_client_test.R;
import com.test003.ggoma.sgr_chat_client_test.utils.GGomaLog;
import com.test003.ggoma.sgr_chat_client_test.utils.RequestHelper;

import org.json.JSONObject;

import java.util.HashMap;

import lab.ggoma.chat.GGomaChatClient;

import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.MESSAGE;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.SELECT;


/**
 * Created by seong-uim on 2017. 5. 30..
 */

public class TestViewController {
    public enum ViewEventType {
        SELECT,
        MESSAGE,

        ROKET,
        FROZEN,
        KICK,
        SHUTUP,
        RECOMM,
        FOLLOW,
        CANDY,
        MEGAPHONE,
        VOTE_START,
        VOTE_END,
        MANAGER_SET,
        MANAGER_UNSET,
        NOTICE,
        ADMIN_MSG,
        ADMIN_KICK,
        EXTERNAL_MESSAGE

    };
    public interface IGGomaChatViewController {
        public void onViewEvent(ViewEventType type, Object... args);
    }
    private Activity mAct;
    private ChatView mChatView = null;
    private int userid_increase = 1;

    private GGomaChatClient chatClient = null;
    private IGGomaChatViewController mInterface = null;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private HashMap<String, User> userMap = new HashMap<String, User>();
    public TestViewController(Activity context, GGomaChatClient client) {
        mAct = context;
        chatClient = client;
    }
    public void setInterface(IGGomaChatViewController inter) {
        mInterface = inter;
    }
    public void popupRoomList(final JSONObject roomData) {
        TestViewDialogHelper.RoomListDialog(mAct, roomData, new TestViewDialogHelper.IUserNoInputDialog() {
            @Override
            public void onInputUserNoAndRoomNo(String userno, String roomno) {
                if(mInterface != null){
                    mInterface.onViewEvent(SELECT, userno, roomno);
                }
            }
        });
    }

    public void setupChatView() {
        mChatView = (ChatView) mAct.findViewById(R.id.chat_view);
        mChatView.setRightBubbleColor(ContextCompat.getColor(mAct, R.color.green500));
        mChatView.setLeftBubbleColor(Color.WHITE);
        mChatView.setBackgroundColor(ContextCompat.getColor(mAct, R.color.blueGray500));
        mChatView.setSendIcon(R.drawable.ic_action_send);
        mChatView.setRightMessageTextColor(Color.WHITE);
        mChatView.setLeftMessageTextColor(Color.BLACK);
        mChatView.setUsernameTextColor(Color.WHITE);
        mChatView.setSendTimeTextColor(Color.WHITE);
        mChatView.setDateSeparatorColor(Color.WHITE);
        mChatView.setInputTextHint("채팅 메시지 입력");
        mChatView.setMessageMarginTop(5);
        mChatView.setMessageMarginBottom(5);

        mChatView.setOnClickOptionButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GGomaLog.d("GGOMA", "setOnClickOptionButtonListener onClick ");
                TestViewDialogHelper.OptionDialog(mAct, chatClient, new TestViewDialogHelper.ISelctedItemDialog() {
                    @Override
                    public void onSelctedItem(Object... args) {
                        ViewEventType type = (ViewEventType)args[0];
                        mInterface.onViewEvent(type, args[1]);
                    }
                });
            }
        });
        mChatView.setOnClickSendButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mChatView.getInputText().isEmpty()){
                    return;
                }
                if(!chatClient.getChatStatusStore().getIsBJMode()) {
                    if (chatClient.getChatStatusStore().getIsFrozen()) {
                        popupToast("얼리기중에는 채팅할 수 없음");
                        return;
                    }
                    if (chatClient.getChatStatusStore().getIsShutup()) {
                        popupToast("벙어리중에는 채팅할 수 없음");
                        return;
                    }
                }
                if(mInterface != null){
                    mInterface.onViewEvent(MESSAGE, chatClient.getChatStatusStore().getmMyUserno(), mChatView.getInputText());
                    mChatView.setInputText("");
                }
            }
        });
        mChatView.setOnIconClickListener(new Message.OnIconClickListener() {
            @Override
            public void onIconClick(Message message) {

                GGomaLog.d("GGOMA", "setOnClickOptionButtonListener onClick " + message.getUser().getName() + " " + message.getMessageText());

                // external 에서  getName()

                // YouTube인 경우에 다이얼로그로 던져서, YouTube차단한다.
                // YT_


            }
        });
    }
    public void addChatUser(JSONObject user){
        if(user.has("userno")) {
            Bitmap userIcon = RequestHelper.getBitmapFromURL(user.optString("thumburl"));
            String role = "일반";
            switch (user.optString("role")){
                case "b": role = "BJ"; break;
                case "m": role = "매니저"; break;
                case "g": role = "일반회원"; break;
                case "a": role = "손님"; break;
            }
            User userUI = new User(userid_increase++, user.optString("usernick") + "|"+ role, userIcon);
            userMap.put(user.optString("userno"), userUI);
            addRoomInfoMessage(userUI.getName() + " 입장");
        }
    }
    public void removeChatUser(JSONObject user){
        if(user.has("userno")){
            addRoomInfoMessage(getCharUser(user.optString("userno")).getName() + " 퇴장");
            userMap.remove(user.optString("userno"));
        }
    }
    public User getCharUser(String userno){
        return userMap.get(userno);
    }
    public void setManagerCharUser(boolean value, String userno){
        String name = userMap.get(userno).getName();
        if(value){
            name = name.replace("일반회원", "매니저");
        }else{
            name = name.replace("매니저", "일반회원");
        }
        userMap.get(userno).setName(name);
    }
    public void addChatMessage(final JSONObject data){
        User user = userMap.get(data.optString("userno"));
        if(user == null) {
            // 이미 나간 유저의 임시 저장 메시지의 경우에
            Bitmap userIcon = RequestHelper.getBitmapFromURL(data.optString("thumburl"));
            user = new User(userid_increase++, data.optString("usernick"), userIcon);
        }
        final User userInfo = user;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Message messageData = new Message.Builder()
                        .setUser(userInfo)
                        .setRightMessage(false)
                        .setMessageText(data.optString("msg"))
                        .hideIcon(false)
                        .build();
                mChatView.send(messageData);
            }
        });
    }
    public void addChatExternalMessage(final JSONObject data){
        // External 맵에 없으면 넣음.
        if(!chatClient.getChatStatusStore().isExistExternalUser(data.optJSONObject("data").optString("userno"))){
            chatClient.getChatStatusStore().addExternalUser(data.optJSONObject("data").optString("userno"), data);
        }

        // 인스턴스 유저 생성.
        Bitmap userIcon = RequestHelper.getBitmapFromURL(data.optJSONObject("data").optString("thumburl"));
        final User externalUser = new User(userid_increase++, "YT_"+data.optJSONObject("data").optString("usernick"), userIcon);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Message messageData = new Message.Builder()
                        .setUser(externalUser)
                        .setRightMessage(false)
                        .setMessageText(data.optJSONObject("data").optString("msg"))
                        .hideIcon(false)
                        .build();
                mChatView.send(messageData);
            }
        });
    }

    public void addEventMessage(final JSONObject data){
        final User user = userMap.get(data.optJSONObject("data").optString("userno"));
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Message messageData = new Message.Builder()
                        .setUser(user)
                        .setRightMessage(true)
                        .setMessageText(data.optString("type") + " " + user.getName() + " 이벤트")
                        .hideIcon(true)
                        .build();
                mChatView.send(messageData);
            }
        });
    }

    public void addRoomInfoMessage(final String message){
        final User user = new User(0, "룸이벤트", null);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Message messageData = new Message.Builder()
                        .setUser(user)
                        .setRightMessage(true)
                        .setMessageText(message)
                        .hideIcon(true)
                        .build();
                mChatView.send(messageData);
            }
        });
    }
    public void errorMessageHandling(final JSONObject error){
        final User user = new User(0, "에러 메시지", null);
        mHandler.post(new Runnable() {
            @Override
            public void run() {

                try{
                    Message messageData = new Message.Builder()
                            .setUser(user)
                            .setRightMessage(true)
                            .setMessageText(error.optString("type") + " " + error.optString("msg"))
                            .hideIcon(true)
                            .build();
                    mChatView.send(messageData);
                }catch (Exception e){
                    GGomaLog.d("GGOMA","errorMessageHandling : " + e.toString());
                }
            }
        });
    }
    private void popupToast(final String message){
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mAct, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}