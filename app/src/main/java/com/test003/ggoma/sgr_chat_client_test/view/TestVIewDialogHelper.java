package com.test003.ggoma.sgr_chat_client_test.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.test003.ggoma.sgr_chat_client_test.utils.GGomaLog;
import com.test003.ggoma.sgr_chat_client_test.utils.HttpAsyncTask;
import com.test003.ggoma.sgr_chat_client_test.utils.RequestHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import lab.ggoma.chat.GGomaChatClient;
import lab.ggoma.chat.GGomaChatConstant;

import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.ADMIN_KICK;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.ADMIN_MSG;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.CANDY;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.EXTERNAL_MESSAGE;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.FOLLOW;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.FROZEN;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.KICK;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.MANAGER_SET;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.MANAGER_UNSET;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.MEGAPHONE;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.NOTICE;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.RECOMM;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.ROKET;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.SELECT;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.SHUTUP;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.VOTE_END;
import static com.test003.ggoma.sgr_chat_client_test.view.TestViewController.ViewEventType.VOTE_START;


/**
 * Created by seong-uim on 2017. 6. 2..
 */

public class TestViewDialogHelper {
    public interface IUserNoInputDialog {
        public void onInputUserNoAndRoomNo(String userno, String roomno);
    }
    public interface IUserSelctedUserDialog {
        public void onSelectedUser(String userno);
    }
    public interface ISelctedItemDialog {
        public void onSelctedItem(Object... args);
    }

    public static void RoomListDialog(final Activity act, final JSONObject roomData, final IUserNoInputDialog handler){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(act);
        alertBuilder.setCancelable(false);
        alertBuilder.setTitle("개설된 룸 리스트");
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_singlechoice);
        JSONArray rooms = roomData.optJSONArray("result");
        for (int i = 0; i < rooms.length(); i++) {
            JSONObject row = rooms.optJSONObject(i);
            String roomNo = row.optString("room");
            String bjNickname = "";
            JSONArray users = row.optJSONArray("users");
            for (int j = 0; j < users.length(); j++) {
                JSONObject user = users.optJSONObject(j);
                if(user.optString("role").equals("b")){
                    bjNickname = user.optString("usernick");
                    break;
                }
            }
            adapter.add(bjNickname + "|" + roomNo);
        }
        alertBuilder.setNegativeButton("신규룸개설", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // 룸개설 모드. => userno 입력 모드로 전달
                UserNoInputDialog(act, null, handler);
            }
        });
        alertBuilder.setPositiveButton("새로고침", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RequestHelper.requestRoomList(new HttpAsyncTask.IHttpAsyncTask() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        if(data != null) {
                            RoomListDialog(act, data ,handler);
                        }
                    }
                });
                dialog.dismiss();

            }
        });
        alertBuilder.setNeutralButton("서버변경", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LinearLayout layout = new LinearLayout(act);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText sererIPBox = new EditText(act);
                sererIPBox.setText(GGomaChatConstant.SERVER_IP);
                layout.addView(sererIPBox);

                final EditText sererPORTBox = new EditText(act);
                sererPORTBox.setText(GGomaChatConstant.SERVER_PORT);
                sererPORTBox.setInputType(InputType.TYPE_CLASS_NUMBER);
                layout.addView(sererPORTBox);


                AlertDialog.Builder builder = new AlertDialog.Builder(act);
                builder.setTitle("서버 정보");
                builder.setView(layout);
                builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String serverip = sererIPBox.getText().toString();
                        String serverport = sererPORTBox.getText().toString();
                        GGomaLog.d("GGOMA", "serverip : " + serverip + " serverport : " + serverport);
                        RoomListDialog(act, roomData ,handler);
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });
        alertBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String roomno = adapter.getItem(id).split("\\|")[1];
                GGomaLog.d("GGOMA", "setAdapter getItem : " + roomno);
                // 개설된 룸의 접속 모드. => userno 입력 모드로 전달
                UserNoInputDialog(act, roomno, handler);
            }
        });
        alertBuilder.show();
    }
    public static void UserNoInputDialog(final Activity act, final String roomno, final IUserNoInputDialog handler) {
        LinearLayout layout = new LinearLayout(act);
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText userNoBox = new EditText(act);
        userNoBox.setHint("userno");
        userNoBox.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(userNoBox);
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle("테스트 유저 넘버 입력(게스트:0)");
        builder.setView(layout);
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userNo = userNoBox.getText().toString();
                GGomaLog.d("GGOMA", "userNo " + userNo);
                if(userNo.isEmpty()){
                    UserNoInputDialog(act, roomno, handler);
                    return;
                }
                if(roomno == null && userNo.equals(GGomaChatConstant.DEFAULT_GUEST_USERNO)){
                    UserNoInputDialog(act, roomno, handler);
                    return;
                }
                if (userNo != null && !userNo.isEmpty()) {
                    if(handler != null){
                        handler.onInputUserNoAndRoomNo(userNo, roomno);
                    }
                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
    public static void OptionDialog(final Activity act, final GGomaChatClient client, final ISelctedItemDialog handler){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(act);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_singlechoice);
        String myRole = client.getChatStatusStore().getMyInfo().optString("role");
        switch (myRole){
            case "b": { // BJ
                alertBuilder.setTitle("BJ권한 기능");
                adapter.add("유저리스트 보기");
                adapter.add("업카운트 입력");
                adapter.add("방얼리기");
                adapter.add("방녹이기");
                adapter.add("강퇴하기");
                adapter.add("벙어리먹이기");
                adapter.add("매니저임명");
                adapter.add("매니저해임");
                adapter.add("투표시작");
                adapter.add("투표종료");
                adapter.add("공지하기");
                adapter.add("어드민메시지");
                adapter.add("어드민강퇴");
                adapter.add("YouTube채팅릴리이테스트");
                break;
            }
            case "m": { // 매니저
                alertBuilder.setTitle("매니저 권한 기능");
                adapter.add("유저리스트 보기");
                adapter.add("업카운트 입력");
                adapter.add("강퇴하기");
                adapter.add("벙어리먹이기");

                adapter.add("추천하기");
                adapter.add("팔로우하기");
                adapter.add("캔디쏘기");
                adapter.add("확성기");
                break;
            }
            case "g": { // 회원
                alertBuilder.setTitle("일반 회원권한 기능");
                adapter.add("유저리스트 보기");
                adapter.add("업카운트 입력");

                adapter.add("추천하기");
                adapter.add("팔로우하기");

                adapter.add("캔디쏘기");
                adapter.add("확성기");
                break;
            }
            case "a": { // 손님
                alertBuilder.setTitle("손님권한 기능");
                adapter.add("유저리스트 보기");
                break;
            }
        }
        alertBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String item = adapter.getItem(id);
                GGomaLog.d("GGOMA", "setAdapter getItem : " + item);

                boolean isUserSelectMode = false;
                TestViewController.ViewEventType eventType = SELECT;
                switch (item){
                    case "유저리스트 보기": {
                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                        builder.setTitle("유저수 : " + client.getChatStatusStore().getUserCount());
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_singlechoice);
                        for( JSONObject userData : client.getChatStatusStore().getUserList() ){
                            String role = "일반";
                            switch (userData.optString("role")){
                                case "b": role = "BJ"; break;
                                case "m": role = "매니저"; break;
                                case "g": role = "일반회원"; break;
                                case "a": role = "손님"; break;
                            }
                            adapter.add(userData.optString("usernick") + "|" + role);
                        }
                        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) { }
                        });
                        builder.show();
                        break;
                    }
                    case "업카운트 입력" : {
                        LinearLayout layout = new LinearLayout(act);
                        layout.setOrientation(LinearLayout.VERTICAL);
                        final EditText upcountBox = new EditText(act);
                        upcountBox.setInputType(InputType.TYPE_CLASS_NUMBER);
                        layout.addView(upcountBox);
                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                        builder.setTitle("업카운트 입력");
                        builder.setView(layout);
                        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String count = upcountBox.getText().toString();
                                GGomaLog.d("GGOMA", "upcountBox " + count);
                                if(handler != null){
                                    handler.onSelctedItem(ROKET, count);
                                }
                            }
                        });
                        builder.show();
                        break;
                    }
                    case "방얼리기" : {
                        if(handler != null){
                            handler.onSelctedItem(FROZEN, true);
                        }
                        break;
                    }
                    case "방녹이기" : {
                        if(handler != null){
                            handler.onSelctedItem(FROZEN, false);
                        }
                        break;
                    }
                    case "강퇴하기" : {
                        eventType = KICK;
                        isUserSelectMode = true;
                        break;
                    }
                    case "추천하기" : {
                        if(handler != null){
                            handler.onSelctedItem(RECOMM, false);
                        }
                        break;
                    }
                    case "팔로우하기" : {
                        if(handler != null){
                            handler.onSelctedItem(FOLLOW, false);
                        }
                        break;
                    }
                    case "벙어리먹이기" : {
                        eventType = SHUTUP;
                        isUserSelectMode = true;
                        break;
                    }
                    case "매니저임명" : {
                        eventType = MANAGER_SET;
                        isUserSelectMode = true;
                        break;
                    }
                    case "매니저해임" :{
                        eventType = MANAGER_UNSET;
                        isUserSelectMode = true;
                        break;
                    }
                    case "투표시작" :{
                        if(handler != null){
                            handler.onSelctedItem(VOTE_START, false);
                        }
                        break;
                    }
                    case "투표종료" :{
                        if(handler != null){
                            handler.onSelctedItem(VOTE_END, false);
                        }
                        break;
                    }
                    case "캔디쏘기" :{
                        LinearLayout layout = new LinearLayout(act);
                        layout.setOrientation(LinearLayout.VERTICAL);
                        final EditText candyCountBox = new EditText(act);
                        candyCountBox.setInputType(InputType.TYPE_CLASS_NUMBER);
                        layout.addView(candyCountBox);
                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                        builder.setTitle("캔디 카운트 입력");
                        builder.setView(layout);
                        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String count = candyCountBox.getText().toString();
                                GGomaLog.d("GGOMA", "candyCountBox " + count);
                                if(handler != null){
                                    handler.onSelctedItem(CANDY, count);
                                }
                            }
                        });
                        builder.show();
                        break;
                    }
                    case "확성기" :{
                        if(handler != null){
                            handler.onSelctedItem(MEGAPHONE, "확성기 메시지 어쩌구");
                        }
                        break;
                    }
                    case "공지하기" :{
                        if(handler != null){
                            handler.onSelctedItem(NOTICE, "공지사항 어쩌구");
                        }
                        break;
                    }
                    case "어드민메시지" :{
                        if(handler != null){
                            handler.onSelctedItem(ADMIN_MSG, "어드민 메시지 어쩌구");
                        }
                        break;
                    }
                    case "어드민강퇴" :{
                        eventType = ADMIN_KICK;
                        isUserSelectMode = true;
                        break;
                    }
                    case "YouTube채팅릴리이테스트" :{
                        if(handler != null){
                            handler.onSelctedItem(EXTERNAL_MESSAGE, "YouTube채팅 메시지 릴레이~");
                        }
                        break;
                    }

                }
                if(isUserSelectMode) {
                    final TestViewController.ViewEventType type = eventType;
                    String myuserno = client.getChatStatusStore().getMyInfo().optString("userno");
                    UserSelectDialog(act, item + " 유저 선택", client, new IUserSelctedUserDialog() {
                        @Override
                        public void onSelectedUser(String userno) {
                            if (handler != null) {
                                handler.onSelctedItem(type, userno);
                            }
                        }
                    });
                }
            }

        });
        alertBuilder.show();
    }
    public static void UserSelectDialog(final Activity act, final String title,  final GGomaChatClient client, final IUserSelctedUserDialog handler) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(title);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_singlechoice);
        String myuserno = client.getChatStatusStore().getMyInfo().optString("userno");
        String bjuserno = client.getChatStatusStore().getMyInfo().optString("userno");

        for( JSONObject userData : client.getChatStatusStore().getUserList() ){
            if (myuserno.equals(userData.optString("userno"))) { // 자기 자신은 뺸다.
                continue;
            }
            if (bjuserno.equals(userData.optString("userno"))) { // bj는 뺀다.
                continue;
            }
            adapter.add(userData.optString("usernick") + "|" + userData.optString("userno"));
        }
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String userno = adapter.getItem(id).split("\\|")[1];
                if (handler != null) {
                    handler.onSelectedUser(userno);
                }
            }
        });
        builder.show();
    }
}
