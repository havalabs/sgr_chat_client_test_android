package com.test003.ggoma.sgr_chat_client_test.utils;

/**
 * Created by seong-uim on 2017. 6. 9..
 */

public class GGomaLog {
    public static void i(String tag, String string) {
        android.util.Log.i(tag, string);
    }
    public static void e(String tag, String string) {
        android.util.Log.e(tag, string);
    }
    public static void d(String tag, String string) {
        android.util.Log.d(tag, string);
    }
    public static void v(String tag, String string) {
        android.util.Log.v(tag, string);
    }
    public static void w(String tag, String string) {
       android.util.Log.w(tag, string);
    }
}